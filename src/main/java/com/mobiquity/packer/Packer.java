package com.mobiquity.packer;

import com.mobiquity.exception.APIException;
import com.mobiquity.models.Item;
import com.mobiquity.models.ItemPackage;

import java.util.ArrayList;
import java.util.List;

import static com.mobiquity.utils.Utils.*;

/**
 * This is packer class , contains one static API
 */
public class Packer {

    private Packer() {
    }

    /**
     * This method accepts the absolute path to a test file as a String.
     * The test file will be in UTF-8 format.
     * The pack method returns the solution as a String.
     * The method should throw an com.mobiquity.exception.APIException if incorrect parameters are being
     * passed.
     *
     * @param filePath is absolute path to a test file as a string
     * @return is a string , a comma separated , multi line, list of index number for valid items
     * @throws APIException thrown in case of incorrect parameters are being passed
     */
    public static String pack(String filePath) throws APIException {
        List<String> indexStrList = new ArrayList<>();
        //This method is for parsing the test file , and creating items and packages information
        List<ItemPackage> itemPackageList = getItemPackageList(filePath);
        //Iterating on parsed package list and finding index list
        for (ItemPackage itemPackage : itemPackageList) {
            //Filtering items , eliminating items with more than package weight
            //Sorting items by ratio of cost/weight per item, for detecting valuable items
            List<Item> itemList = sortAnfFilter(itemPackage.getItemList(),
                    getBigDecimal(itemPackage.getWeight()));
            //adding valuable items sequentially , till package weight limit
            List<Integer> indexList = getSelectedItemIndexList(itemList,
                    getBigDecimal(itemPackage.getWeight()));
            //Generating a comma separated result by index list
            String indexListStr = getResult(indexList);
            indexStrList.add(indexListStr);
        }
        //Merging all result lines
        return mergeResult(indexStrList);
    }

}
