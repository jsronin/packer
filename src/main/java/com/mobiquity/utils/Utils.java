package com.mobiquity.utils;

import com.mobiquity.exception.APIException;
import com.mobiquity.models.Item;
import com.mobiquity.models.ItemPackage;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This is utils for parsing and validating data
 */
public class Utils {

    public static final int SCALE = 2;
    public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
    public static final String CURRENCY = "€";
    public static final Integer MAX_PACKAGE_WEIGHT = 100;
    public static final BigDecimal MAX_ITEM_WEIGHT = BigDecimal.valueOf(100);
    public static final Integer MAX_ITEM_COST = 100;
    public static final Integer MIN_PACKAGE_WEIGHT = 0;
    public static final Integer MAX_ITEM_COUNT = 15;

    /**
     * This API is for getting absolute path of a file in resources folder
     *
     * @param fileName is file name
     * @return is absolute path
     */
    public static String getAbsolutePath(String fileName) {
        try {
            return Utils.class.getClassLoader().getResource(fileName).getPath().substring(1);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * This API is for removing currency symbol from the string
     *
     * @param str is given string
     * @return is result string without the symbol
     */
    public static String removeCurrency(String str) {
        if (str == null || str.length() <= 0) {
            return "";
        } else {
            return str.replaceAll(CURRENCY, "").trim();
        }
    }

    /**
     * This API is for getting the weight of a package from data line
     *
     * @param line is data line
     * @return is integer value of the weight
     */
    public static Integer getWeight(String line) {
        return getInteger(line.split(":")[0].trim());
    }

    /**
     * This API is for getting item from data string
     *
     * @param itemStr is data string
     * @return is item entity
     */
    public static Item getItem(String itemStr) {
        try {
            String[] itemParamList = itemStr.trim().substring(1, itemStr.length() - 1).split(",");
            Item result = new Item(getInteger(itemParamList[0]),
                    getBigDecimal(itemParamList[1]),
                    getInteger(removeCurrency(itemParamList[2])));
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * This API is for getting item list string from data line
     *
     * @param line is data line
     * @return is item list string
     */
    public static String getItemListStr(String line) {
        try {
            return line.split(":")[1];
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * This API is for getting item list from data string
     *
     * @param itemListStr is data string
     * @return is item entity list
     */
    public static List<Item> getItemList(String itemListStr) {
        List<Item> result = new ArrayList<>();
        try {
            for (String itemStr : itemListStr.trim().split(" ")) {
                Item item = getItem(itemStr);
                if (item != null) {
                    result.add(item);
                } else {
                    return null;
                }
            }
        } catch (Exception e) {
            return null;
        }
        return result;
    }

    /**
     * This API is for getting package list from given path of file
     *
     * @param path is file path
     * @return is a list of package entity
     * @throws APIException thrown in case of incorrect parameters are being passed
     */
    public static List<ItemPackage> getItemPackageList(String path) throws APIException {
        List<ItemPackage> result = new ArrayList<>();
        for (String line : getFileLines(path)) {
            Integer weight = getWeight(line);
            if (!checkMaxPackageWeight(weight)) {
                throw new APIException("Invalid package weight : " + weight);
            }
            List<Item> itemList = getItemList(getItemListStr(line));
            if (!checkItemCount(itemList)) {
                throw new APIException("Invalid item count : " + itemList.size());
            }
            if (!checkMaxItemWeightAndCost(itemList)) {
                throw new APIException("Invalid item weight or cost ");
            }
            ItemPackage itemPackage = new ItemPackage(weight, itemList);
            result.add(itemPackage);
        }
        return result;
    }

    /**
     * This API is for getting text content of a file (lines) as a list string
     *
     * @param path is absolute path of the file
     * @return is a list of lines
     * @throws APIException thrown in case of incorrect parameters are being passed
     */
    public static List<String> getFileLines(String path) throws APIException {
        try {
            return Files.lines(Paths.get(path)).collect(Collectors.toList());
        } catch (Exception e) {
            throw new APIException(e.getMessage(), e);
        }
    }

    /**
     * This API is for filtering items with more than package limit weight
     * and sorting items by cost/weight
     *
     * @param itemList is a list of items
     * @param weight   is weight limit of the package
     * @return is a list of sorted and filtered items
     */
    public static List<Item> sortAnfFilter(List<Item> itemList,
                                           BigDecimal weight) {
        return itemList.stream().filter(i -> i.getWeight().compareTo(weight) <= 0).
                sorted((i1, i2) ->
                        BigDecimal.valueOf(i2.getCost()).divide(i2.getWeight(), SCALE, ROUNDING_MODE).
                                compareTo(BigDecimal.valueOf(i1.getCost()).divide(i1.getWeight(), SCALE, ROUNDING_MODE))).
                collect(Collectors.toList());
    }

    /**
     * This API is for finding item indexes (valuable items) till weight limit
     *
     * @param itemList is a list of items
     * @param weight   is the weight limit
     * @return is a list of item index
     */
    public static List<Integer> getSelectedItemIndexList(List<Item> itemList,
                                                         BigDecimal weight) {
        List<Integer> result = new ArrayList<>();
        BigDecimal currentLoad = BigDecimal.ZERO;
        for (Item item : itemList) {
            BigDecimal freeSpace = weight.subtract(currentLoad);
            if (item.getWeight().compareTo(freeSpace) <= 0) {
                currentLoad = currentLoad.add(item.getWeight());
                result.add(item.getIndex());
            }
        }
        return result.stream().sorted().collect(Collectors.toList());
    }

    /**
     * This API is for generating a comma separated result by index list
     *
     * @param indexList is a list of items
     * @return is a comma separated string of result indexes
     */
    public static String getResult(List<Integer> indexList) {
        if (indexList == null || indexList.size() <= 0) {
            return "-";
        }
        StringBuilder stringBuilder = new StringBuilder();
        indexList.forEach(i -> stringBuilder.append(i.toString() + ","));
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }

    /**
     * This API is for merging result lines to a single result string
     *
     * @param indexList is a result string list
     * @return is a single multi line result string
     */
    public static String mergeResult(List<String> indexList) {
        if (indexList == null || indexList.size() <= 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        indexList.forEach(i -> stringBuilder.append(i + "\n"));
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }

    /**
     * This API is for converting integer to big decimal
     *
     * @param intValue is a integer value
     * @return is a converted big decimal
     */
    public static BigDecimal getBigDecimal(Integer intValue) {
        try {
            return BigDecimal.valueOf(intValue);
        } catch (Exception e) {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * This API is for converting string to big decimal
     *
     * @param bigDecimalStr is a string value
     * @return is a converted big decimal
     */
    public static BigDecimal getBigDecimal(String bigDecimalStr) {
        try {
            return new BigDecimal(bigDecimalStr);
        } catch (Exception e) {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * This API is for converting string to integer
     *
     * @param intStr is a string value
     * @return is a converted big decimal
     */
    public static Integer getInteger(String intStr) {
        try {
            return Integer.valueOf(intStr);
        } catch (Exception e) {
            throw new NumberFormatException(e.getMessage());
        }
    }

    /**
     * This API is for checking package weight limit
     *
     * @param weight is package weight
     * @return is a boolean , in case of success is true , false for vice versa
     */
    public static Boolean checkMaxPackageWeight(Integer weight) {
        if (weight != null &&
                weight >= MIN_PACKAGE_WEIGHT &&
                weight <= MAX_PACKAGE_WEIGHT) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This API is for checking max item count limit
     *
     * @param itemList is item list
     * @return is a boolean , in case of success is true , false for vice versa
     */
    public static Boolean checkItemCount(List<Item> itemList) {
        if (itemList == null || itemList.size() > MAX_ITEM_COUNT) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * This API is for checking max item weight and cost
     *
     * @param itemList is item list
     * @return is a boolean , in case of success is true , false for vice versa
     */
    public static Boolean checkMaxItemWeightAndCost(List<Item> itemList) {
        if (itemList == null) {
            return false;
        }
        if (itemList.stream().filter(i -> i.getWeight().compareTo(MAX_ITEM_WEIGHT) <= 0).count() !=
                itemList.size()) {
            return false;
        }
        if (itemList.stream().filter(i -> i.getCost() <= MAX_ITEM_COST).count() !=
                itemList.size()) {
            return false;
        }
        return true;
    }

}
