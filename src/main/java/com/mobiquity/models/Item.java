package com.mobiquity.models;

import com.google.gson.Gson;

import java.math.BigDecimal;

/**
 * This is item entity
 */
public class Item {

    private Integer index;
    private BigDecimal weight;
    private Integer cost;

    public Item() {
    }

    public Item(Integer index,
                BigDecimal weight,
                Integer cost) {
        this.index = index;
        this.weight = weight;
        this.cost = cost;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Item))
            return false;
        Item other = (Item) o;
        return this.index.equals(other.index) &&
                this.weight.equals(other.weight) &&
                this.cost.equals(other.cost);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (index != null) {
            result = 31 * result + index.hashCode();
        }
        if (cost != null) {
            result = 31 * result + cost.hashCode();
        }
        return result;
    }

}
