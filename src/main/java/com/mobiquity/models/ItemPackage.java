package com.mobiquity.models;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * This is item package entity
 */
public class ItemPackage {

    private Integer weight;
    private List<Item> itemList;

    public ItemPackage() {
    }

    public ItemPackage(Integer weight,
                       List<Item> itemList) {
        this.weight = weight;
        this.itemList = itemList;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public void addItem(Item item) {
        if (itemList == null) {
            itemList = new ArrayList<>();
        }
        itemList.add(item);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
