package com.mobiquity.exception;

/**
 * This is a custom exception in case of incorrect parameters are being passed.
 */
public class APIException extends Exception {

    public APIException(String message, Exception e) {
        super(message, e);
    }

    public APIException(String message) {
        super(message);
    }
}
